using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class VendasController : ControllerBase
  {
    private readonly DadosVendasContext _context;

    public VendasController(DadosVendasContext context)
    {
      _context = context;
    }

    [HttpPost]
    public IActionResult Registrar(Venda venda)
    {
      if (venda == null)
        return BadRequest("Venda não pode ser vazia");

      venda.Status = EnumStatusVenda.Aguardando_pagamento;
      _context.Venda.Add(venda);
      _context.SaveChanges();

      return Ok(venda);
    }

    [HttpGet("{id}")]
    public IActionResult BuscarPorId(int id)
    {
      var venda = _context.Venda.Include(v => v.Vendedor.Id).Include(x => x.Produto.Id).FirstOrDefault(v => v.Id == id);

      if (venda == null)
        return NotFound("Venda não encontrada");

      return Ok(venda);
    }

    [HttpPut("{id}")]
    public IActionResult AtualizarVenda(int id, EnumStatusVenda status)
    {
      var result = false;
      var mensagem = "";
      var venda = _context.Venda.Find(id);

      if (venda == null)
      {
        return NotFound("Venda não encontrada");
      }
      else
      {
        switch (venda.Status)
        {
          case EnumStatusVenda.Aguardando_pagamento:
            {
              if ((status == EnumStatusVenda.Pagamento_aprovado) || (status == EnumStatusVenda.Cancelada))
              {
                result = true;
                venda.Status = status;
                break;
              }
              else
              {
                mensagem = "Operação negada. Por favor selecione 'Pagamento Aprovado' ou 'Cancelada'";
                break;
              }
            }
          case EnumStatusVenda.Pagamento_aprovado:
            {
              if ((status == EnumStatusVenda.Enviado_para_transportadora) || (status == EnumStatusVenda.Cancelada))
              {
                result = true;
                venda.Status = status;
                break;
              }
              else
              {
                mensagem = "Operação negada. Por favor selecione 'Enviado para Transportadora' ou 'Cancelada'";
                break;
              }
            }
          case EnumStatusVenda.Enviado_para_transportadora:
            {
              if (status == EnumStatusVenda.Entregue)
              {
                result = true;
                venda.Status = status;
                break;
              }
              else
              {
                mensagem = "Operação negada. Por favor selecione 'Entregue'";
                break;
              }
            }
          case EnumStatusVenda.Entregue:
            {
              mensagem = "Operação negada. A venda já foi entregue e não pode mais ser alterada";
              break;
            }
          default:
            {
              mensagem = "Operação negada. O status da venda não pode ser vazio";
              break;
            }

        }
      }

      if (result)
      {
        _context.Venda.Update(venda);
        _context.SaveChanges();
        return Ok($"Status da venda {id} alterado com sucesso");
      }
      else
      {
        return BadRequest(mensagem);
      }
    }
  }
}