namespace tech_test_payment_api.Models
{
  public class Venda
  {
    public int Id { get; set; }
    public Vendedor Vendedor { get; set; }
    public DateTime Data { get; set; }
    public Produto Produto { get; set; }
    public EnumStatusVenda Status { get; set; }
  }
}